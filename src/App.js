import React from 'react'
import MessageList from './components/MessageList.js';
import SendMessageForm from './components/SendMessageForm.js';
import RoomList from './components/RoomList.js';
import NewRoomForm from './components/NewRoomForm.js';
import { tokenUrl, instanceLocator } from "./config.js";
import  { ChatManager, TokenProvider } from '@pusher/chatkit-client';



class App extends React.Component {
  constructor(){
    super();
    this.state = {
      messages: [],
      joinedRooms: [],
      joinableRooms: [],
      currentRoom: null,
      
    };
  }

  sendMessage = (text) => {
    this.currentUser.sendSimpleMessage({
      text,
      roomId: this.state.currentRoom
    });
  }


  subscribeToRoom = (roomId) => {
    this.setState({messages:[], currentRoom: roomId})
    this.currentUser.subscribeToRoomMultipart({
      roomId,
      messageLimit: 30,
      hooks: {
        onMessage: message => {
          this.setState({
            messages: [...this.state.messages, message]
          });
        }
      }
    })
    .then(room =>{
      this.getRooms();
    })
  }

  getRooms = () => {
    this.currentUser.getJoinableRooms()
      .then(joinableRooms => {
        this.setState({
          joinableRooms,
          joinedRooms: this.currentUser.rooms
        });
      })
      .catch(err => console.log('error on joinableRooms: ', err));
  }

  componentDidMount(){
    const chatManager = new ChatManager({
      instanceLocator,
      userId: 'ata',
      tokenProvider: new TokenProvider({
        url: tokenUrl
      })
    });
    
    chatManager.connect()
    .then(currentUser => {
      this.currentUser = currentUser;      
      // console.log('status', this.currentUser);
       this.getRooms();
    })
     .catch(err => {
       console.error("error in conection:", err);
     });
  }

  creatRoom = (name) =>{
    this.currentUser.createRoom({
      name
    })
    .then(room => this.subscribeToRoom(room.id))
    .catch(err => console.log('error in create new room',err))
  }
  
    render() {

        return (
            <div className="app">
                <RoomList 
                  rooms={[...this.state.joinableRooms,...this.state.joinedRooms]} 
                  roomHandler={this.subscribeToRoom} 
                />
                <MessageList
                  roomId={this.state.currentRoom}
                  tex={this.state.messages}/>
                <SendMessageForm
                  disabled={!this.state.currentRoom}
                  send={this.sendMessage} />
                <NewRoomForm createRoom={this.creatRoom} />
            </div>
        );
    }
}

export default App;