import React from 'react'

const Message = (props) => {
   
    return(
        <div className="message">
            <div className="message-username" style={props.status === "online" ? {color: 'red'} : {color :'gray'}}>{props.senderName}</div>
            <div className="message-text" >{props.textOfMessage}</div>
        </div>
    );
}


export default Message;
