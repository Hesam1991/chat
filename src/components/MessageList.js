import React from 'react';
import ReactDOM from 'react-dom';
import Message from "./Message.js";

class MessageList extends React.Component {

    componentWillUpdate(){
        const node = ReactDOM.findDOMNode(this);
        this.shouldScrollToBottom = node.scrollTop + node.clientHeight + 100 >= node.scrollHeight;
    }

    componentDidUpdate() {
        if (this.shouldScrollToBottom){
            const node = ReactDOM.findDOMNode(this);
            node.scrollTop = node.scrollHeight;
        }
    }
    
    render() {
        if(!this.props.roomId){
            return(
                <div className="message-list">
                    <div className="join-room">
                        &larr; Join a room!
                    </div>
                </div>
            );
        }
    const fullMessages =  this.props.tex.map(each => {
        // console.log('sender_name', each.userStore.users.hesam);
        console.log('ogjiotgiot', each);
    return(<Message
                key={each.id} 
                sender_id={each.senderId} 
                senderName={each.userStore.users[each.senderId].name} 
                textOfMessage={each.parts[0].payload.content}
                status={each.userStore.presenceStore[each.senderId]} 
                />);
     });
        
        return (
            <div className="message-list">
                {fullMessages}
            </div>
        );
    }
}

export default MessageList;