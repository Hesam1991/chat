import React from 'react';

class SendMessageForm extends React.Component {
    state = {
        message: ''
    };

    handleMessage = (e) => {      
        this.setState({
            message: e.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.send(this.state.message);
        this.setState({
            message : ''
        });
    }
    render() {
        return (
            <form className="send-message-form" onSubmit={this.handleSubmit}>
                <input
                    disabled={this.props.disabled}
                    placeholder="SendMessageForm"
                    type="text" 
                    value={this.state.message} 
                    onChange = {this.handleMessage} />
            </form>
        );
    }
}

export default SendMessageForm;