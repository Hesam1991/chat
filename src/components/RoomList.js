import React from 'react';

class RoomList extends React.Component {
    
    render () {
        this.props.rooms.sort((a, b) =>  (a.name.toUpperCase() >  b.name.toUpperCase() ? 1 : -1) );
        return (
            <div className="rooms-list">
                <div className="help-text">RoomList</div>
                <ul>
                    {this.props.rooms
                        .map(room => <li key={room.id} className='room'>
                                     <button onClick={() => this.props.roomHandler(room.id)}>{room.name}</button>
                                     </li>)}
                </ul>
            </div>
        );
    }
}

export default RoomList;